$(document).ready(function () {
  jQuery('.img-inline').each(function(){
    var $img = jQuery(this);
    var imgID = $img.attr('id');
    var imgClass = $img.attr('class');
    var imgURL = $img.attr('src');

    jQuery.get(imgURL, function(data) {
      // Get the SVG tag, ignore the rest
      var $svg = jQuery(data).find('svg');

      // Add replaced image's ID to the new SVG
      if(typeof imgID !== 'undefined') {
        $svg = $svg.attr('id', imgID);
      }
      // Add replaced image's classes to the new SVG
      if(typeof imgClass !== 'undefined') {
        $svg = $svg.attr('class', imgClass+' replaced-svg');
      }

      // Remove any invalid XML tags as per http://validator.w3.org
      $svg = $svg.removeAttr('xmlns:a');

      // Replace image with new SVG
      $img.replaceWith($svg);

    }, 'xml');

  });

});

// Dynamic copyright
$('.copyright-year').html(new Date().getFullYear());

// Material ripple-effect animation
(function($) {
  $(".ripple-effect").click(function(e){
    var rippler = $(this);

    // create .ink element if it doesn't exist
    if(rippler.find(".ink").length === 0) {
      rippler.append("<span class='ink'></span>");
    }

    var ink = rippler.find(".ink");

    // prevent quick double clicks
    ink.removeClass("animate");

    // set .ink diametr
    if(!ink.height() && !ink.width())
    {
      var d = Math.max(rippler.outerWidth(), rippler.outerHeight());
      ink.css({height: d, width: d});
    }

    // get click coordinates
    var x = e.pageX - rippler.offset().left - ink.width()/2;
    var y = e.pageY - rippler.offset().top - ink.height()/2;

    // set .ink position and add class .animate
    ink.css({
      top: y+'px',
      left:x+'px'
    }).addClass("animate");
  });
})(jQuery);


// Material specific delay for css3 transitions
(function($) {
  var speed = 2200;
  var container =  $('.display-animation');  
  container.each(function() {   
    var elements = $(this).children();
    elements.each(function() {      
      var elementOffset = $(this).offset(); 
      var offset = elementOffset.left*0.8 + elementOffset.top;
      var delay = parseFloat(offset/speed).toFixed(2);
      $(this)
        .css("transition-delay", delay+'s')
        .addClass('animated');
    });
  });
})(jQuery);

$(".motivation li").click(function() {
  $(this).toggleClass('active');
});

