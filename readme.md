#VisualForce Bootstrap Base
###Built for:
VisualForce

####Development prerequisites:
+ NPM
+ Bower
+ Ruby Sass

####Viewing prerequisites:
+ A browser that can handle HTML Imports:
[Check for support](http://caniuse.com/#feat=imports)


####Packages (managed by Bower):
+ Bootstrap Sass - 3.3.5
+ Bourbon - 4.2.3
+ Neat - 1.7.2
---

##Please note:
####Editing CSS files directly is a bad life decision!
If you must do such a thing, please create a separate branch.
With the exception of some third-party plug-ins or frameworks, all of the CSS has been compiled from the SCSS files located in `/dev/stylesheets` folder.

